import express from 'express';

const app=express();
let puerto=3000;

app.get("/sumar", function(req,res){
    let resultado=Number(req.query.a)+Number(req.query.b);
    res.send(resultado.toString());
}
);
app.get("/restar", function(req,res){
    let resultado=Number(req.query.a)-Number(req.query.b);
    res.send(resultado.toString());
}
);
app.get("/dividir", function(req,res){
    let resultado=Number(req.query.a)/Number(req.query.b);
    if(req.query.b==0){
        res.send("No se puede dividir entre 0")
    }else{
    res.send(resultado.toString());
    }
}
);
app.get("/multiplicar", function(req,res){
    let resultado=Number(req.query.a)*Number(req.query.b);
    res.send(resultado.toString());
}
);
app.get("/modulo", function(req,res){
    let resultado=Number(req.query.a)%Number(req.query.b);
    res.send(resultado.toString());
}
);
app.listen(puerto,function(){
    console.log("server "+puerto)
})